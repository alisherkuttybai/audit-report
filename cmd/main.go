package main

import (
	"log"

	"git.magnum.kz/1010.axsa/audit-report/internal/app"
	"git.magnum.kz/1010.axsa/audit-report/internal/config"
)

func main() {
	cfg, err := config.New()
	if err != nil {
		log.Fatalf("failed init config: %s", err)
	}
	application, err := app.New(cfg)
	if err != nil {
		log.Fatalf("failed init application: %s", err)
	}
	application.Run()
}
