package config

import (
	"fmt"
	"os"
	"strings"
	"time"

	"github.com/ilyakaznacheev/cleanenv"
)

type Config struct {
	App   AppCfg   `yaml:"APP" env:"APP"`
	GRPC  GRPCCfg  `yaml:"GRPC" env:"GRPC"`
	DB    DBCfg    `yaml:"DB" env:"DB"`
	Minio MinioCfg `yaml:"MINIO" env:"MINIO"`
}

type AppCfg struct {
	LogLevel string `yaml:"LOG_LEVEL" env:"APP_LOG_LEVEL" env-required:"true"`
	Name     string `yaml:"NAME" env:"APP_NAME" env-required:"true"`
}

type GRPCCfg struct {
	Host    string        `yaml:"HOST" env:"GRPC_PORT" env-required:"true"`
	Port    string        `yaml:"PORT" env:"GRPC_HOST" env-required:"true"`
	Timeout time.Duration `yaml:"TIMEOUT" env:"GRPC_TIMEOUT" env-required:"true"`
}

type DBCfg struct {
	Host     string `yaml:"HOST" env:"DB_PORT" env-required:"true"`
	Port     string `yaml:"PORT" env:"DB_HOST" env-required:"true"`
	Keyspace string `yaml:"KEYSPACE" env:"DB_KEYSPACE" env-required:"true"`
	User     string `yaml:"USER" env:"DB_USER" env-required:"true"`
	Password string `yaml:"PASSWORD" env:"DB_PASSWORD" env-required:"true"`
}

type MinioCfg struct {
	Url        string `yaml:"URL" env:"MINIO_URL" env-required:"true"`
	BucketName string `yaml:"BUCKET_NAME" env:"MINIO_BUCKET_NAME" env-required:"true"`
	AccessKey  string `yaml:"ACCESS_KEY" env:"MINIO_ACCESS_KEY" env-required:"true"`
	SecretKey  string `yaml:"SECRET_KEY" env:"MINIO_SECRET_KEY" env-required:"true"`
}

func New() (*Config, error) {
	cfg := &Config{}

	cfgPath := os.Getenv("CONFIG_PATH")
	if strings.TrimSpace(cfgPath) == "" {
		cfgPath = "./config/config.yaml"
	}

	if _, err := os.Stat(cfgPath); os.IsNotExist(err) {
		return nil, fmt.Errorf("config file does not exist: %s", cfgPath)
	}

	if err := cleanenv.ReadConfig("./config/config.yaml", cfg); err != nil {
		return nil, fmt.Errorf("failed read config: %w", err)
	}

	return cfg, nil
}
