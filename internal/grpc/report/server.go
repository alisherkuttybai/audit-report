package report

import (
	"context"
	"time"

	reportmodel "git.magnum.kz/1010.axsa/audit-report/internal/model/report"
	"git.magnum.kz/1010.axsa/audit-report/pkg/logger"
	auditv1 "git.magnum.kz/1010.axsa/audit-report/protos/gen/go/audit/v1"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type ReportService interface {
	SaveReport(ctx context.Context, file string, report *reportmodel.Report) error
	GetReports() ([]reportmodel.Report, error)
}

type serverAPI struct {
	auditv1.UnimplementedReportServer

	log    *logger.Logger
	report ReportService
}

func New(log *logger.Logger, gRPC *grpc.Server, r ReportService) {
	auditv1.RegisterReportServer(gRPC, &serverAPI{log: log, report: r})
}

func (s *serverAPI) SaveReport(ctx context.Context, req *auditv1.SaveReportRequest) (*auditv1.SaveReportResponse, error) {
	if req.ObjectName == "" {
		return nil, status.Error(codes.InvalidArgument, "object_name is required")
	}
	if req.UserId == "" {
		return nil, status.Error(codes.InvalidArgument, "user_id is required")
	}
	if req.DateBegin == "" {
		return nil, status.Error(codes.InvalidArgument, "data_begin is required")
	}
	if req.DateEnd == "" {
		return nil, status.Error(codes.InvalidArgument, "data_end is required")
	}

	dtBegin, err := time.Parse("01-02-2006", req.GetDateBegin())
	if err != nil {
		s.log.Error("failed parse time", logger.Err(err))
		return nil, status.Error(codes.Internal, "internal error")
	}
	dtEnd, err := time.Parse("01-02-2006", req.GetDateEnd())
	if err != nil {
		s.log.Error("failed parse time", logger.Err(err))
		return nil, status.Error(codes.Internal, "internal error")
	}

	report := &reportmodel.Report{
		ObjectName: req.GetObjectName(),
		UserId:     req.GetUserId(),
		DateBegin:  dtBegin,
		DateEnd:    dtEnd,
	}

	if err := s.report.SaveReport(ctx, req.File, report); err != nil {
		return nil, status.Error(codes.Internal, "internal error")
	}

	return &auditv1.SaveReportResponse{
		Status:  int64(codes.OK),
		Message: req.GetObjectName(),
	}, nil
}

func (s *serverAPI) GetReports(ctx context.Context, req *auditv1.GetReportsRequest) (*auditv1.GetReportsResponse, error) {
	reports, err := s.report.GetReports()
	if err != nil {
		return nil, status.Error(codes.Internal, "internal error")
	}

	var protoReports []*auditv1.ReportObject
	for _, report := range reports {
		protoReport := reportmodel.СonvertToProtoReport(report)
		protoReports = append(protoReports, protoReport)
	}

	return &auditv1.GetReportsResponse{
		Objects: protoReports,
	}, nil
}
