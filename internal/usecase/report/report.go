package report

import (
	"bytes"
	"context"
	"encoding/base64"
	"fmt"
	"net/url"
	"time"

	reportmodel "git.magnum.kz/1010.axsa/audit-report/internal/model/report"
	"git.magnum.kz/1010.axsa/audit-report/pkg/logger"
	"github.com/gocql/gocql"
)

type ReportUsecase struct {
	log         *logger.Logger
	cassandraDB CassandraDB
	saver       ReportSaver
	provider    ReportProvider
	s3          StorageS3
}

type CassandraDB interface {
	GetConn() *gocql.Session
}

type ReportSaver interface {
	SaveReport(ctx context.Context, report *reportmodel.Report) error
}

type ReportProvider interface {
	GetReports() ([]reportmodel.Report, error)
}

type StorageS3 interface {
	UploadFile(ctx context.Context, objectName string, reader *bytes.Reader) error
	GetFileLink(ctx context.Context, objectName string, reader *bytes.Reader) (*url.URL, error)
}

func New(log *logger.Logger, db CassandraDB, saver ReportSaver, provider ReportProvider, sender StorageS3) *ReportUsecase {
	return &ReportUsecase{
		log:         log,
		cassandraDB: db,
		saver:       saver,
		provider:    provider,
		s3:          sender,
	}
}

func (r *ReportUsecase) SaveReport(ctx context.Context, file string, report *reportmodel.Report) error {
	data, err := base64.StdEncoding.DecodeString(file)
	if err != nil {
		r.log.Error("failed decoding base64", logger.Err(err))
		return fmt.Errorf("failed decoding base64: %w", err)
	}
	reader := bytes.NewReader(data)

	report.DateInsert = time.Now()
	report.FileName = fmt.Sprintf("%s-%s", report.ObjectName, report.DateInsert.Format("01-02-2006"))

	if err := r.s3.UploadFile(ctx, report.FileName, reader); err != nil {
		r.log.Error("failed to upload file", logger.Err(err))
		return fmt.Errorf("failed to upload file: %w", err)
	}

	url, err := r.s3.GetFileLink(ctx, report.FileName, reader)
	if err != nil {
		r.log.Error("failed to get file link", logger.Err(err))
		return fmt.Errorf("failed to get file link: %w", err)
	}

	report.FileLink = fmt.Sprintf("%s://%s%s", url.Scheme, url.Host, url.Path)

	if err := r.saver.SaveReport(ctx, report); err != nil {
		r.log.Error("failed save report", logger.Err(err))
		return fmt.Errorf("failed save report: %w", err)
	}

	return nil
}

func (r *ReportUsecase) GetReports() ([]reportmodel.Report, error) {
	reports, err := r.provider.GetReports()
	if err != nil {
		r.log.Error("failed get reports", logger.Err(err))
		return nil, fmt.Errorf("failed get reports: %w", err)
	}

	return reports, nil
}
