package usecase

import (
	"git.magnum.kz/1010.axsa/audit-report/internal/repository"
	"git.magnum.kz/1010.axsa/audit-report/internal/usecase/report"
	"git.magnum.kz/1010.axsa/audit-report/pkg/logger"
	"git.magnum.kz/1010.axsa/audit-report/pkg/minio"
)

type UsecaseSet struct {
	ReportUsecase *report.ReportUsecase
}

func New(log *logger.Logger, db report.CassandraDB, repoSet *repository.RepositorySet, minio *minio.Minio) *UsecaseSet {
	return &UsecaseSet{
		ReportUsecase: report.New(log, db, repoSet.ReportRepository, repoSet.ReportRepository, minio),
	}
}
