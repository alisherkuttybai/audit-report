package report

import (
	"context"
	"fmt"

	reportmodel "git.magnum.kz/1010.axsa/audit-report/internal/model/report"
	"git.magnum.kz/1010.axsa/audit-report/pkg/logger"
	"github.com/gocql/gocql"
	"github.com/scylladb/gocqlx"
	"github.com/scylladb/gocqlx/qb"
)

type ReportRepository struct {
	log     *logger.Logger
	session *gocql.Session
}

func New(log *logger.Logger, s *gocql.Session) *ReportRepository {
	return &ReportRepository{
		log:     log,
		session: s,
	}
}

func (r *ReportRepository) SaveReport(ctx context.Context, report *reportmodel.Report) error {
	query := qb.Insert("audit.reports").Columns(
		"object_name",
		"user_id",
		"date_begin",
		"date_end",
		"file_name",
		"file_link",
		"date_insert",
		"state",
	)

	m := qb.M{
		"object_name": report.ObjectName,
		"user_id":     report.UserId,
		"date_begin":  report.DateBegin,
		"date_end":    report.DateEnd,
		"file_name":   report.FileName,
		"file_link":   report.FileLink,
		"date_insert": report.DateInsert,
		"state":       report.State,
	}

	stmt, names := query.Unique().ToCql()
	q := gocqlx.Query(r.session.Query(stmt), names).BindMap(m)
	if err := q.Err(); err != nil {
		return fmt.Errorf("failed binding: %w", err)
	}

	if err := q.Exec(); err != nil {
		return fmt.Errorf("failed exec: %w", err)
	}

	return nil
}

func (r *ReportRepository) GetReports() ([]reportmodel.Report, error) {
	query := qb.Select("audit.reports").Columns(
		"object_name",
		"date_begin",
		"date_end",
		"file_name",
		"file_link",
		"date_insert",
		"state",
	)

	stmt, names := query.Limit(500).AllowFiltering().ToCql()
	q := gocqlx.Query(r.session.Query(stmt), names)
	if err := q.Err(); err != nil {
		return nil, fmt.Errorf("failed binding: %w", err)
	}

	iter := gocqlx.Iter(q.Query)
	defer iter.Close()

	reports := []reportmodel.Report{}
	report := reportmodel.Report{}

	for iter.Scan(
		&report.ObjectName,
		&report.DateBegin,
		&report.DateEnd,
		&report.FileName,
		&report.FileLink,
		&report.DateInsert,
		&report.State) {
		reports = append(reports, report)
	}

	return reports, nil
}
