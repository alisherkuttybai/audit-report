package repository

import (
	"git.magnum.kz/1010.axsa/audit-report/internal/repository/report"
	"git.magnum.kz/1010.axsa/audit-report/pkg/logger"
	"github.com/gocql/gocql"
)

type RepositorySet struct {
	Session          *gocql.Session
	ReportRepository *report.ReportRepository
}

func New(log *logger.Logger, s *gocql.Session) *RepositorySet {
	return &RepositorySet{
		Session:          s,
		ReportRepository: report.New(log, s),
	}
}
