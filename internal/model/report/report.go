package report

import (
	"time"

	auditv1 "git.magnum.kz/1010.axsa/audit-report/protos/gen/go/audit/v1"
)

type Report struct {
	ObjectName string    `json:"object_name"`
	UserId     string    `json:"user_id"`
	DateBegin  time.Time `json:"date_begin"`
	DateEnd    time.Time `json:"date_end"`
	FileName   string    `json:"file_name"`
	FileLink   string    `json:"file_link"`
	DateInsert time.Time `json:"date_insert"`
	State      int       `json:"state"`
}

func СonvertToProtoReport(report Report) *auditv1.ReportObject {
	return &auditv1.ReportObject{
		ObjectName: report.ObjectName,
		DateBegin:  report.DateBegin.String(),
		DateEnd:    report.DateEnd.String(),
		FileName:   report.FileName,
		FileLink:   report.FileLink,
		DateInsert: report.DateInsert.String(),
		State:      int64(report.State),
	}
}
