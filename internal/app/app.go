package app

import (
	"fmt"
	"log/slog"
	"os"
	"os/signal"
	"syscall"

	"git.magnum.kz/1010.axsa/audit-report/internal/config"
	reportgrpc "git.magnum.kz/1010.axsa/audit-report/internal/grpc/report"
	"git.magnum.kz/1010.axsa/audit-report/internal/repository"
	"git.magnum.kz/1010.axsa/audit-report/internal/usecase"
	"git.magnum.kz/1010.axsa/audit-report/pkg/grpcserver"
	"git.magnum.kz/1010.axsa/audit-report/pkg/logger"
	"git.magnum.kz/1010.axsa/audit-report/pkg/minio"
	"git.magnum.kz/1010.axsa/audit-report/pkg/scylla"
)

type App struct {
	log     *logger.Logger
	db      *scylla.ScyllaDB
	minio   *minio.Minio
	grpcsrv *grpcserver.GRPCServer
}

func New(cfg *config.Config) (*App, error) {
	log, err := logger.New(cfg.App.LogLevel)
	if err != nil {
		return nil, fmt.Errorf("failed init logger: %w", err)
	}
	db, err := scylla.New(&cfg.DB, log)
	if err != nil {
		return nil, fmt.Errorf("failed init database: %w", err)
	}
	minio, err := minio.New(&cfg.Minio)
	if err != nil {
		return nil, fmt.Errorf("failed init file storage: %w", err)
	}

	repoSet := repository.New(log, db.Session)
	uscSet := usecase.New(log, db, repoSet, minio)
	grpcsrv := grpcserver.New(&cfg.GRPC)

	reportgrpc.New(log, grpcsrv.Server, uscSet.ReportUsecase)

	return &App{
		log:     log,
		db:      db,
		minio:   minio,
		grpcsrv: grpcsrv,
	}, nil
}

func (a *App) Run() {
	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	a.log.Info("application starting")
	a.log.Info("application started")

	go func() {
		err := a.grpcsrv.Start()
		if err != nil {
			a.log.Error("failed start grpc server", logger.Err(err))
		}
	}()

	sign := <-stop
	a.log.Info("stopping application", slog.String("signal", sign.String()))

	a.Stop()
	a.log.Info("application stopped")
}

func (a *App) Stop() {
	a.db.Stop()
	a.grpcsrv.Stop()
}
