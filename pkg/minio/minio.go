package minio

import (
	"bytes"
	"context"
	"fmt"
	"net/url"
	"time"

	"git.magnum.kz/1010.axsa/audit-report/internal/config"
	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
)

type Minio struct {
	client *minio.Client
	params *config.MinioCfg
}

func New(cfg *config.MinioCfg) (*Minio, error) {
	client, err := minio.New(cfg.Url, &minio.Options{
		Creds:  credentials.NewStaticV4(cfg.AccessKey, cfg.SecretKey, ""),
		Secure: false,
	})
	if err != nil {
		return nil, fmt.Errorf("failed to create minio client: %w", err)
	}

	return &Minio{
		client: client,
		params: cfg,
	}, nil
}

func (m *Minio) UploadFile(ctx context.Context, objectName string, reader *bytes.Reader) error {
	_, err := m.client.PutObject(ctx, m.params.BucketName, objectName, reader, -1, minio.PutObjectOptions{})
	if err != nil {
		return fmt.Errorf("failed to upload file to minio: %w", err)
	}

	return nil
}

func (m *Minio) GetFileLink(ctx context.Context, objectName string, reader *bytes.Reader) (*url.URL, error) {
	reqParams := make(map[string][]string)
	expiry := 604800 * time.Second
	url, err := m.client.PresignedGetObject(ctx, m.params.BucketName, objectName, expiry, reqParams)
	if err != nil {
		return nil, fmt.Errorf("failed to upload file to minio: %w", err)
	}

	return url, nil
}
