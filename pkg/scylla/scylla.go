package scylla

import (
	"fmt"
	"strconv"

	"git.magnum.kz/1010.axsa/audit-report/internal/config"
	"git.magnum.kz/1010.axsa/audit-report/pkg/logger"

	"github.com/gocql/gocql"
)

type ScyllaDB struct {
	Session *gocql.Session
}

func New(cfg *config.DBCfg, log *logger.Logger) (*ScyllaDB, error) {
	var err error

	c := gocql.NewCluster(cfg.Host)
	c.Port, err = strconv.Atoi(cfg.Port)
	if err != nil {
		return nil, fmt.Errorf("failed to convert to int: %w", err)
	}
	c.Logger = log
	c.Keyspace = cfg.Keyspace
	c.Authenticator = gocql.PasswordAuthenticator{
		Username: cfg.User,
		Password: cfg.Password,
	}

	s, err := c.CreateSession()
	if err != nil {
		return nil, fmt.Errorf("failed to connect to scylla: %w", err)
	}

	return &ScyllaDB{
		Session: s,
	}, nil
}

func (s *ScyllaDB) GetConn() *gocql.Session {
	return s.Session
}

func (s *ScyllaDB) Stop() {
	s.Session.Close()
}
