package grpcserver

import (
	"fmt"
	"net"

	"git.magnum.kz/1010.axsa/audit-report/internal/config"
	"google.golang.org/grpc"
)

type GRPCServer struct {
	Server  *grpc.Server
	address string
}

func New(cfg *config.GRPCCfg) *GRPCServer {
	return &GRPCServer{
		Server:  grpc.NewServer(),
		address: fmt.Sprintf("%s:%s", cfg.Host, cfg.Port),
	}
}

func (srv *GRPCServer) Start() error {
	l, err := net.Listen("tcp", srv.address)
	if err != nil {
		return fmt.Errorf("failed init database: %w", err)
	}
	if err := srv.Server.Serve(l); err != nil {
		return fmt.Errorf("failed init database: %w", err)
	}

	return nil
}

func (srv *GRPCServer) Stop() {
	srv.Server.GracefulStop()
}
